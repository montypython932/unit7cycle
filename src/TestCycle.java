import java.util.EmptyStackException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TestCycle {

	static double wheels, weight;
	
	public static void main(String[] args) throws Exception {
		Scanner input = new Scanner(System.in);
		try {
			wheels = input.nextDouble();
			weight = input.nextDouble();
			if (wheels <= 0 || weight <=0) {
				throw new EmptyStackException();
			} 
		} catch (InputMismatchException e) {
			System.out.println("That isn't a number");
		} catch (EmptyStackException e) {
			System.out.println("Error, that number is <= 0");
		}
		Cycle cyc1 = new Cycle();
		Cycle cyc2 = new Cycle(wheels, weight);
		System.out.println(cyc1.toString());
		System.out.println(cyc2.toString());
	}

}